#!/bin/sh

if [ $# -ne 1 -o ! -f "$1" ]; then
 echo "ERROR: You must specify an existing filename"
 echo
 echo "Usage:"
 echo "  $0 <filename>"
 exit 1
fi

FILE=$1

gpg --output $FILE.gpg -r "Fixmo Cloud Management" -e $FILE

rm -f $FILE

git add $FILE.gpg

git commit -m "adding $FILE.gpg"

git fetch origin

git rebase

git push origin
