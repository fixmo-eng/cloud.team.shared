#!/bin/sh

if [ $# -ne 1 ]; then
 echo "ERROR: You must specify a filename"
 echo
 echo "Usage:"
 echo "  $0 <new_filename>"
 exit 1
fi

if ! [ -e "$1" ]; then
 echo "ERROR: File does not exist!"
 exit 2
fi

FILE=$1

git stash

gpg --output $FILE.edit -d $FILE

vi $FILE.edit

gpg --output $FILE -r "Fixmo Cloud Management" -e $FILE.edit

rm $FILE.edit 

git add $FILE

git commit 

git fetch origin

git rebase origin

git push origin

git stash pop
