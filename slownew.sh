#!/bin/sh

if [ $# -ne 1 ]; then
 echo "ERROR: You must specify a filename"
 echo
 echo "Usage:"
 echo "  $0 <new_filename>"
 exit 1
fi

git stash

FILE=$1

vi $FILE.new

gpg --output $FILE.gpg -r "Fixmo Cloud Management" -e $FILE.new

rm $FILE.new

git add $FILE.gpg

git commit 

git fetch origin

git rebase origin

git push origin

git stash pop
