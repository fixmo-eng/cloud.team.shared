#!/bin/sh

if [ $# -ne 1 ]; then
 echo "ERROR: You must specify a filename"
 echo
 echo "Usage:"
 echo "  $0 <new_filename>"
 exit 1
fi

if ! [ -e "$1" ]; then
 echo "ERROR: File does not exist!"
 exit 2
fi

FILE=$1

gpg --output $FILE.out -d $FILE

echo
echo
echo "###############################"
echo
cat $FILE.out
echo
echo "###############################"
echo
echo

rm -f $FILE.out

