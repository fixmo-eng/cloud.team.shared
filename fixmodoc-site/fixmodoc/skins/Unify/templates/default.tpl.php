<!DOCTYPE html>
<html xmlns="<?php $this->text('xhtmldefaultnamespace') ?>" xml:lang="<?php $this->text('lang') ?>" lang="<?php $this->text('lang') ?>" dir="<?php $this->text('dir') ?>">
<head>
	<meta http-equiv="Content-Type" content="<?php $this->text('mimetype') ?>; charset=<?php $this->text('charset') ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<?php $this->html('headlinks') ?>
	<title><?php $this->text('pagetitle') ?></title>
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Montserrat" />
	<link rel="stylesheet" type="text/css" href="<?php $this->text('stylepath') ?>/WikiDocs/bootstrap/css/bootstrap.min.css?<?php echo $GLOBALS['wgStyleVersion'] ?>" />
	<link rel="stylesheet" type="text/css" href="<?php $this->text('stylepath') ?>/WikiDocs/bootstrap/css/bootstrap-responsive.min.css?<?php echo $GLOBALS['wgStyleVersion'] ?>" />
	<link rel="stylesheet" type="text/css" href="<?php $this->text('stylepath') ?>/WikiDocs/font-awesome/css/font-awesome.min.css?<?php echo $GLOBALS['wgStyleVersion'] ?>" />
  <link rel="stylesheet" type="text/css" href="<?php $this->text('stylepath') ?>/Unify/css/headers/header1.css?<?php echo $GLOBALS['wgStyleVersion'] ?>" />
  <link rel="stylesheet" type="text/css" href="<?php $this->text('stylepath') ?>/Unify/css/style.css?<?php echo $GLOBALS['wgStyleVersion'] ?>" />
  <link rel="stylesheet" type="text/css" href="<?php $this->text('stylepath') ?>/Unify/css/style_responsive.csstweaks.css?<?php echo $GLOBALS['wgStyleVersion'] ?>" />
  <link rel="stylesheet" type="text/css" href="<?php $this->text('stylepath') ?>/Unify/css/themes/red.css?<?php echo $GLOBALS['wgStyleVersion'] ?>" />
  <link rel="stylesheet" type="text/css" href="<?php $this->text('stylepath') ?>/Unify/css/themes/headers/header1-red.css?<?php echo $GLOBALS['wgStyleVersion'] ?>" />
  <link rel="stylesheet" type="text/css" href="<?php $this->text('stylepath') ?>/Unify/plugins/bootstrap-select/bootstrap-select.min.css?<?php echo $GLOBALS['wgStyleVersion'] ?>" />
	<link rel="stylesheet" type="text/css" href="<?php $this->text('stylepath') ?>/Unify/css/tweaks.css?<?php echo $GLOBALS['wgStyleVersion'] ?>" />
  <link rel="stylesheet" type="text/css" href="<?php $this->text('stylepath') ?>/Unify/css/tweaks_responsive.css?<?php echo $GLOBALS['wgStyleVersion'] ?>" />
	<?php print Skin::makeGlobalVariablesScript($this->data); ?>
	<script type="text/javascript" src="<?php $this->text('stylepath') ?>/WikiDocs/jquery.js"></script>
	<?php $this->html('headscripts'); ?>
	<?php print $wgOut->getScript(); ?>
	<script type="text/javascript">
		function ponyDocsOnLoad() {}

		function AjaxChangeProduct_callback( o ) {
			document.getElementById('docsProductSelect').disabled = true;
			var s = new String( o.responseText );
			document.getElementById('docsProductSelect').disabled = false;
			window.location.href = s;
		}

		function AjaxChangeProduct( ) {
			var productIndex = document.getElementById('docsProductSelect').selectedIndex;
			var product = document.getElementById('docsProductSelect')[productIndex].value;
			var title = '<?php $this->jstext('thispage'); ?>';
			sajax_do_call( 'efPonyDocsAjaxChangeProduct', [product,title], AjaxChangeProduct_callback );
		}

		function AjaxChangeVersion_callback( o ) {
			document.getElementById('docsVersionSelect').disabled = true;
			var s = new String( o.responseText );
			document.getElementById('docsVersionSelect').disabled = false;
			window.location.href = s;
		}

		function AjaxChangeVersion( ) {
			var productIndex = document.getElementById('docsProductSelect').selectedIndex;
			var product = document.getElementById('docsProductSelect')[productIndex].value;
			var versionIndex = document.getElementById('docsVersionSelect').selectedIndex;
			var version = document.getElementById('docsVersionSelect')[versionIndex].value;
			var title = '<?php $this->jstext('thispage'); ?>';
			sajax_do_call( 'efPonyDocsAjaxChangeVersion', [product,version,title], AjaxChangeVersion_callback );
		}

		function changeManual(){
			var url = $("#docsManualSelect").val();
			if (url != ""){
				window.location.href = url;
			}
		}
	</script>
</head>
<body <?php if($this->data['body_ondblclick']) { ?>ondblclick="<?php $this->text('body_ondblclick') ?>"<?php } ?>
	<?php if($this->data['body_onload']) { ?>onload="<?php $this->text('body_onload') ?>"<?php } ?>
	class="mediawiki <?php $this->text('nsclass') ?> <?php $this->text('dir') ?> <?php $this->text('pageclass') ?>">


<!--=== Top ===-->    
<div class="top">
  <div class="container">         
    <ul class="loginbar pull-right">
      <li><a href="http://fixmo.com">Fixmo</a></li>
      <li><a href="http://fixmo.com/blog">Blog</a></li>
      <li class="last"><a href="http://support.fixmo.com/">Support</a></li>  
    </ul>
  </div>
</div><!--/top-->
<!--=== End Top ===-->


<!--=== Header ===-->
<div class="header">
  <div class="container">
    <!-- Logo -->
    <div class="logo pull-left">
      <a href="/"><img id="logo-header" src="/skins/Unify/logo.png" alt="Logo"></a>
    </div><!-- /logo -->

    <!-- Menu -->
    <div class="navbar">
      <div class="navbar-inner">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a><!-- /nav-collapse -->
        <div class="nav-collapse collapse">
          <?php print $this->htmlAdminMenu(); ?>
        </div><!-- /nav-collapse -->
      </div><!-- /navbar-inner -->
    </div><!-- /navbar -->
  </div><!-- /container -->
</div><!--/header -->
<!--=== End Header ===-->

<!--=== Breadcrumbs ===-->
<div class="breadcrumbs margin-bottom-20">
  <div class="container">
    <div class="pull-left">
      <?php print $this->data['headertext']; ?>
      <?php print $this->productChangeHtml(); ?>
      <?php print $this->htmlProductChangeVersion(); ?>
    </div>
    <div class="pull-right">
      <?php print $this->generateBreadcrumbMenu(); ?>
    </div>
  </div><!--/container-->
</div><!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->


<!--=== Content Part ===-->
<div id="content">
  <div id="content-inner" class="container">
    <div class="row-fluid">
      <div class="span12">
        <?php print $this->documentationStatus(); ?>

        <?php print $this->phpErrors(); ?>

        <?php if($this->data['sitenotice']) { ?><div id="siteNotice"><?php $this->html('sitenotice') ?></div><?php } ?>

        <?php $this->html('bodytext') ?>

        <?php print $this->generateTopicPager(); ?>

        <?php print $this->affectedVersionsHtml(); ?>
      </div><!--/span9-->
    </div><!--/row-fluid-->
  </div><!--/container-->
</div><!--/content-->
<!--=== End Content Part ===-->

<!--=== Footer ===-->
<div class="footer">
  <div class="container"> 
    <div class="row-fluid">
      <div class="span3">
        <div class="headline"><h3>Fixmo Links</h3></div>
        <ul>
          <li><a href="http://fixmo.com/why-fixmo">Why Fixmo</a></li>
          <li><a href="http://fixmo.com/products">Our Products</a></li>
          <li><a href="http://fixmo.com/resources">Resources</a></li>
          <li><a href="http://fixmo.com/company">Company</a></li>
        </ul>
      </div>

      <?php print $this->htmlProductList(); ?>

      <div class="span6">
        <div class="headline"><h3>Stay Connected</h3></div>	
  			<ul class="social-icons">
  				<li><a href="http://www.facebook.com/fixmo" data-original-title="Facebook" class="social_facebook"></a></li>
  				<li><a href="http://twitter.com/fixmo" data-original-title="Twitter" class="social_twitter"></a></li>
  				<li><a href="http://plus.google.com/fixmo" data-original-title="Google Plus" class="social_googleplus"></a></li>
  				<li><a href="http://www.linkedin.com/company/fixmo" data-original-title="Linkedin" class="social_linkedin"></a></li>
  				<li><a href="http://www.youtube.com/user/FixmoMRM" data-original-title="YouTube" class="social_youtube"></a></li>
  			</ul>
  		</div>
    </div><!--/row-fluid-->	
  </div><!--/container-->	
</div><!--/footer-->	
<!--=== End Footer ===-->

<!--=== Copyright ===-->
<div class="copyright">
  <div class="container">
    <div class="row-fluid">
      <div class="span8">
        <p>2009-2013 &copy; Fixmo, Inc. ALL Rights Reserved. <a href="http://fixmo.com/privacy-policy">Privacy Policy</a> | <a href="http://fixmo.com/terms-of-service">Terms of Service</a></p>
      </div>
      <div class="span4">	
        <a href="/"><img id="logo-footer" src="/skins/Unify/logo.png" class="pull-right" alt="" /></a>
      </div>
    </div><!--/row-fluid-->
  </div><!--/container-->	
</div><!--/copyright-->	
<!--=== End Copyright ===-->

<script type="text/javascript" src="/skins/WikiDocs/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="/skins/WikiDocs/sauce.js"></script>
<script type="text/javascript" src="/skins/Unify/js/app.js"></script>

<?php $this->html('bottomscripts'); /* JS call to runBodyOnloadHook */ ?>
<?php $this->html('reporttime') ?>

<?php 
if ($this->data['debug']) {
	$this->text( 'debug' );
}
?>

</body>
</html>
