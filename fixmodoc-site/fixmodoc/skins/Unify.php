<?php

require_once("WikiDocs.php");

class SkinUnify extends PonyDocsSkinTemplate {
	var $skinname = 'Unify';
	var $stylename = 'Unify';
	var $template = 'UnifyTemplate';
	var $useHeadElement = true;

	function setupSkinUserCss( OutputPage $out ) {
		parent::setupSkinUserCss( $out );

		$out->addStyle( 'http://fonts.googleapis.com/css?family=Montserrat');
		$out->addStyle( 'WikiDocs/bootstrap/css/bootstrap.min.css');
		$out->addStyle( 'WikiDocs/bootstrap/css/bootstrap-responsive.min.css');
		$out->addStyle( 'WikiDocs/font-awesome/css/font-awesome.min.css');
		$out->addStyle( 'WikiDocs/main.css' );

		$out->addStyle( 'Unify/tweaks.css' );

		$out->addMeta('viewport', 'width=device-width, initial-scale=1.0');
		
		$out->addLink( array('rel' => 'icon', 'type' => 'image/x-icon', 'href' => '/skins/Unify/favicon.ico') );
	}

	function tocIndent() {
		return "\n<ul class=\"nav nav-list\">";
	}

	/**
	 * Finish one or more sublevels on the Table of Contents
	 */
	function tocUnindent($level) {
		return "</li>\n" . str_repeat( "</ul>\n</li>\n", $level>0 ? $level : 0 );
	}

	/**
	 * End a Table Of Contents line.
	 * tocUnindent() will be used instead if we're ending a line below
	 * the new level.
	 */
	function tocLineEnd() {
		return "</li>\n";
 	}
	

	function tocLine( $anchor, $tocline, $tocnumber, $level, $sectionIndex = false ) {
		$classes = "toclevel-$level";
		if ( $sectionIndex !== false )
			$classes .= " tocsection-$sectionIndex";
		return "\n<li class=\"$classes\"><a href=\"#" .
			$anchor . '"><span class="tocnumber">' . '</span> <span class="toctext">' .
			$tocline . '</span></a>';
	}

	function tocList($toc) {
		$title = wfMsgHtml('toc') ;

		$output =<<<EOL
			<div id="contents" class="pull-right well well-small">
				<ul class="nav nav-list">
					<li class="nav-header">{$title}</li>
					<li>{$toc}</li>
				</ul>
			</div>
EOL;

		return $output;
	}
	
}


class UnifyTemplate extends WikiDocsTemplate {
	public function userMenu() {
		$output = parent::userMenu();

		$output = str_replace("btn-primary", "btn-danger", $output);
		
		return $output;
	}
	
	public function searchForm() {
		$output = parent::searchForm();

		$output = str_replace("btn-info", "", $output);

		return $output;
	}
	
	public function productChangeHtml() {
		$items = array();
		foreach (PonyDocsProduct::GetDefinedProducts() as $product) {
			$selected = '';
			if ($product->getShortName() == $this->data['selectedProduct']) {
				$selected = ' selected="selected"';
			}
			$items[] = '<option value="'.$product->getShortName().'"'.$selected.'>'.$product->getLongName().'</option>';
		}

		$output_items = implode("\n", $items);

		$output =<<<EOL
			<div id="productChange" class="product" style="display: none;">';
				<select id="docsProductSelect" onchange="AjaxChangeProduct();" name="selectedProduct" class="selectpicker" data-style="btn-inverse">
					{$output_items}
				</select>
			</div>
EOL;
		return $output;
	}
	
	public function htmlProductChangeVersion() {
		$versions = PonyDocsProductVersion::GetVersionsForUser($this->data['selectedProduct']);
		
		if (empty($versions))
			return '';

		foreach ($versions as $version) {
			$latest = PonyDocsProductVersion::GetLatestVersion($this->data['selectedProduct'])->getVersionName();
			$extra = '';
			$selected = '';
			if ($this->isAdmin()) {
				$extra .= ' [' . $version->getVersionStatus() . ']';
			}
			if ($latest == $version->getVersionName()) {
				$extra .= ' ('.wfMsgForContent('histlast').')';
			}
			if ($this->data['selectedVersion'] == $version->getVersionName()) {
				$selected = ' selected="selected"';
			}
			
			$items[] = '<option value="'.$version->getVersionName().'"'.$selected.'>'.$version->getVersionName().$extra.'</option>';
		}

		$output_items = implode("\n", $items);

		$output =<<<EOL
			<div id="productVersionChange" class="productVersion">
				<input type="hidden" name="selectedProduct" value="{$this->data['selectedProduct']}" />
				<select id="docsVersionSelect" name="selectedVersion" onChange="AjaxChangeVersion();" class="selectpicker" data-type="btn-inverse">
					{$output_items}
				</select>
			</div>
EOL;

		return $output;
	}

  public function htmlProductList() {
    $output =<<<EOL
<div class="span3">
  <div class="headline"><h3>Product Docs</h3></div>	
  <ul>
    <li><a href="#">Enterprise</a></li>
    <li><a href="#">Government</a></li>
    <li><a href="#">Wireless Service Providers</a></li>
    <li><a href="#">Developers</a></li>
  </ul>
</div><!--/span4-->	
EOL;

    return $output;
  }

  public function htmlRecentChanges() {
    $output =<<<EOL
<div class="span6">
	<div class="posts">
		<div class="headline"><h3>Recent Documentation Changes</h3></div>
		<dl class="dl-horizontal">
			<dt><a href="#"><img src="assets/img/sliders/elastislide/6.jpg" alt="" /></a></dt>
			<dd>
				<p>From the Blog -- <a href="#">Anim moon officia Unify is an incredibly beautiful responsive Bootstrap Template</a></p> 
			</dd>
		</dl>
		<dl class="dl-horizontal">
			<dt><a href="#"><img src="assets/img/sliders/elastislide/10.jpg" alt="" /></a></dt>
			<dd>
				<p>From the Press -- <a href="#">Anim moon officia Unify is an incredibly beautiful responsive Bootstrap Template</a></p> 
			</dd>
		</dl>
		<dl class="dl-horizontal">
			<dt><a href="#"><img src="assets/img/sliders/elastislide/11.jpg" alt="" /></a></dt>
			<dd>
				<p>From the Media -- <a href="#">Anim moon officia Unify is an incredibly beautiful responsive Bootstrap Template</a></p> 
			</dd>
		</dl>
	</div>
</div><!--/span4-->
EOL;

    return $output;
  }



  public function navigationMenu() {
		$dropdown = $this->generateNavDropdownHTML($this->navigationMenu);

		$output =<<<EOL
<div class="navigation nav-collapse collapse">
	{$dropdown}
</div>
EOL;

		return $output;
	}


  public function generateNavDropdownHTML($menu, $submenu = false, $level = 1) {
		$output = array();

		if ($level > 1) {
			$output[] = '<li class="dropdown '.$menu['class'].'">';
			$output[] = '<a class="dropdown-toggle" data-toggle="dropdown" href="'. $menu['url'] .'">' . $menu['label'] . ' <b class="caret"></b></a>';
			$output[] = '<ul class="dropdown-menu">';
		}
		else {
			$output[] = '<ul class="nav">';
		}

		foreach ($menu['items'] as $item) {
			if (isset($item['divider']) && $level == 1) {
				//$output[] = ' <li class="divider-vertical"></li>';
			}
			else if (isset($item['url']) && isset($item['items'])) {
				$output[] = $this->generateNavDropdownHTML($item, true, $level + 1);
			}
			else if (isset($item['url'])) {
				// URL
				$output[] = ' <li class="'.$item['class'].'"><a href="'. $item['url'] . '">' . $item['label'] . '</a></li>';
			}
			else if (isset($item['divider'])) {
				$output[] = ' <li class="divider"></li>';
			}
			else {
				// Nav Header
				$output[] = ' <li class="nav-header">' . $item['label'] . '</li>';
			}
		}
		
		if ($submenu == false) {
		  $output[] = '<li class="last"><a class="search"><i class="icon-search search-btn"></i></a></li>';
			$output[] = '</ul>';
		}
		else {
			$output[] = '</ul>';
			$output[] = '</li>';
		}
		
		return implode("\n", $output);
	}
	
	
	public function generateManualTocHtml() {
		if (!sizeof($this->data['manualtoc']))
			return '';

    $x = 0;
		foreach ( $this->data['manualtoc'] as $idx => $data ) {
			if ($data['level'] == 0) {
				$output[] = '<li class="nav-header">'.$data['text'].'</li>';
				$x = 0;
			}
			else if ($data['level'] == 1) {
        if ($x == 0) {
          $classes = ' first';
        } else {
          $classes = '';
        }

				if ($data['current']) {
					$output[] = '<li class="active'.$classes.'"><a href="'.$data['link'].'">'.$data['text'].'</a></li>';
				}
				else {
					$output[] = '<li class="'.$classes.'"><a href="'.$data['link'].'">'.$data['text'].'</a></li>';
				}

				$x++;
			}
			else {
				if ($data['current']) {
					$output[] = '<li class="active"><a href="'.$data['link'].'">'.$data['text'].'</a></li>';
				}
				else {
					$output[] = '<li><a href="'.$data['link'].'">'.$data['text'].'</a></li>';
				}
			}
		}

		$output[] = '<li class="divider"></li>';
		$output[] = '<li><a href="/index.php?title='.$this->globals->wgTitle->__toString().'&action=pdfbook"><i class="icon-book"></i> PDF '.wfMsgForContent('version').'</a></li>';

		$manual_output = implode("\n", $output);

		$output =<<<EOL
			<div class="section-menu">
				<ul class="nav nav-list">
					{$manual_output}
				</ul>
			</div>
EOL;

		return $output;
	}



  public function generateBreadcrumbMenu() {
		$items = array();

		for ($x=0; $x<count($this->breadcrumbMenu); $x++) {
			$item = $this->breadcrumbMenu[$x];

			if ($x == count($this->breadcrumbMenu)-1) {
				//$items[] = '<li class="active">'.$item['label'].'</li>';
			}
			else {
				if (isset($item['url'])) {
					if ($x == count($this->breadcrumbMenu)-2) {
					  $items[] = '<li><a href="'.$item['url'].'">'.$item['label'].'</a></li>';
					}
					else {
					  $items[] = '<li><a href="'.$item['url'].'">'.$item['label'].'</a> <span class="divider">/</span></li>';
					}
				}
				else {
				  if ($x == count($this->breadcrumbMenu)-2) {
				    $items[] = '<li class="active">'.$item['label'].'</li>';
				  }
				  else {
				    $items[] = '<li class="active">'.$item['label'].' <span class="divider">/</span></li>';
				  }
					
				}
			}
		}

		$item_output = implode("\n", $items);

		$output =<<<EOL
<div id="breadcrumbs" class="hidden-phone">
	<ul class="breadcrumb">
		{$item_output}
	</ul>
</div>
EOL;

		return $output;
	}


  public function htmlAdminMenu() {
		if ($this->globals->wgUser->isLoggedIn() && $this->isAdmin()) {
			$dropdown = $this->generateDropdownHTML($this->adminMenu);
			$output =<<<EOL
<div class="btn-group admin-menu visible-desktop">
	<a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#">
		Admin <span class="caret"></span>
	</a>
	{$dropdown}
</div>
EOL;
		} else {
			$output = '';
		}

		return $output;
	}


}
