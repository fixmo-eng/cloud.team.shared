<?php

require_once("WikiDocs.php");

class SkinFixmo extends PonyDocsSkinTemplate {
	var $skinname = 'Fixmo';
	var $stylename = 'Fixmo';
	var $template = 'FixmoTemplate';
	var $useHeadElement = true;

	function setupSkinUserCss( OutputPage $out ) {
		parent::setupSkinUserCss( $out );

		$out->addStyle( 'http://fonts.googleapis.com/css?family=Montserrat');
		$out->addStyle( 'WikiDocs/bootstrap/css/bootstrap.min.css');
		$out->addStyle( 'WikiDocs/bootstrap/css/bootstrap-responsive.min.css');
		$out->addStyle( 'WikiDocs/font-awesome/css/font-awesome.min.css');
		$out->addStyle( 'WikiDocs/main.css' );

		$out->addStyle( 'Fixmo/tweaks.css' );

		$out->addMeta('viewport', 'width=device-width, initial-scale=1.0');
		
		$out->addLink( array('rel' => 'icon', 'type' => 'image/x-icon', 'href' => '/skins/Fixmo/favicon.ico') );
	}

	function tocIndent() {
		return "\n<ul class=\"nav nav-list\">";
	}

	/**
	 * Finish one or more sublevels on the Table of Contents
	 */
	function tocUnindent($level) {
		return "</li>\n" . str_repeat( "</ul>\n</li>\n", $level>0 ? $level : 0 );
	}

	/**
	 * End a Table Of Contents line.
	 * tocUnindent() will be used instead if we're ending a line below
	 * the new level.
	 */
	function tocLineEnd() {
		return "</li>\n";
 	}
	

	function tocLine( $anchor, $tocline, $tocnumber, $level, $sectionIndex = false ) {
		$classes = "toclevel-$level";
		if ( $sectionIndex !== false )
			$classes .= " tocsection-$sectionIndex";
		return "\n<li class=\"$classes\"><a href=\"#" .
			$anchor . '"><span class="tocnumber">' . '</span> <span class="toctext">' .
			$tocline . '</span></a>';
	}

	function tocList($toc) {
		$title = wfMsgHtml('toc') ;

		$output =<<<EOL
			<div id="contents" class="pull-right well well-small">
				<ul class="nav nav-list">
					<li class="nav-header">{$title}</li>
					<li>{$toc}</li>
				</ul>
			</div>
EOL;

		return $output;
	}
	
}


class FixmoTemplate extends WikiDocsTemplate {
	public function userMenu() {
		$output = parent::userMenu();

		$output = str_replace("btn-primary", "btn-danger", $output);
		
		return $output;
	}
	
	public function searchForm() {
		$output = parent::searchForm();

		$output = str_replace("btn-info", "", $output);

		return $output;
	}
}
