# Overview

# Scripts 

## Adding a File

bash new.sh <filename>

files can be in the root directory in a folder

## Editing a File

bash edit.sh <filename>

## Reading a File

bash read.sh <filename> 



# Manual

## Encrypting

## Decrypting

There is no need to decrypt and save the contents to a file. To decrypt use the following command

gpg -d _filename_

The contents will be outputted to the screen.

