#!/bin/bash
kato process list --all logyard | grep logyard | sed 's/logyard//g' | sed 's/RUNNING//g' > /tmp/out #process list, remove information around IP addresses.  Could customize for any process
sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]//g" /tmp/out > /tmp/out2 #remove color from process list output 
tr -d ' \t' </tmp/out2 >/tmp/address #trim whitespace
while read line
do				#Any patch instructions could go here.
	echo ssh stackato@$line
	ssh -n stackato@$line 'cp /s/go/bin/logyard /s/go/bin/logyard.bak;wget https://dl.dropboxusercontent.com/u/87045/tmp/bug98997/logyard-v2;kato process stop logyard;cp logyard-v2 /s/go/bin/logyard;kato process start logyard;rm logyard-v2' 
done </tmp/address
rm /tmp/out /tmp/out2 /tmp/address
